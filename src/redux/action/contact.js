import {showMessage} from '../../utils';
import {setLoading} from './global';

const {default: Axios} = require('axios');
const {API_HOST} = require('../../config');

export const getContactListData = () => dispatch => {
  dispatch(setLoading(true));

  Axios.get(`${API_HOST.url}/contact`)
    .then(res => {
      dispatch({type: 'GET_CONTACT', value: res.data.data});
      dispatch(setLoading(false));
    })
    .catch(err => {
      showMessage(err.response.data.message, 'erorr');
      
      dispatch(setLoading(false));
    });
};

export const createContactData = (dataContact, navigation) => dispatch => {
  dispatch(setLoading(true));

  Axios.post(`${API_HOST.url}/contact`, JSON.stringify(dataContact), {
    headers: {'Content-Type': 'application/json'},
  })
    .then(res => {
      dispatch(setLoading(false));
      showMessage(res.data.message, 'success');
      navigation.reset({index: 0, routes: [{name: 'Home'}]});
    })
    .catch(err => {
      showMessage(err.response.data.message, 'erorr');

      dispatch(setLoading(false));
    });
};

export const updateContactData = (id, dataContact, navigation) => dispatch => {
  dispatch(setLoading(true));

  Axios.put(`${API_HOST.url}/contact/` + id, JSON.stringify(dataContact), {
    headers: {'Content-Type': 'application/json'},
  })
    .then(res => {
      dispatch(setLoading(false));
      showMessage(res.data.message, 'success');
      navigation.reset({index: 0, routes: [{name: 'Home'}]});
    })
    .catch(err => {
      showMessage(err.response.data.message, 'erorr');

      dispatch(setLoading(false));
    });
};

export const deleteContactData = id => dispatch => {
  dispatch(setLoading(true));

  Axios.delete(`${API_HOST.url}/contact/` + id)
    .then(res => {
      showMessage(res.data.message, 'success');
      dispatch(setLoading(false));
    })
    .catch(err => {
      showMessage(err.response.data.message, 'erorr');
      dispatch(setLoading(false));
    });
};

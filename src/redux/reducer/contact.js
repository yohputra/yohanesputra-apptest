const initContact = {
  data: [],
};

export const contactReducer = (state = initContact, action) => {
  if (action.type === 'GET_CONTACT') {
    return {
      ...state,
      data: action.value,
    };
  }
  return state;
};

const initCreateContact = {
  firstName: '',
  lastName: '',
  age: '',
  photo: '',
};

export const createContactReducer = (state = initCreateContact, action) => {
  if (action.type === 'CREATE_CONTACT') {
    return {
      ...state,
      firstName: action.value.firstName,
      lastName: action.value.lastName,
      age: action.value.age,
      photo: action.value.photo,
    };
  }
  return state;
};
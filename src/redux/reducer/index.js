import {combineReducers} from 'redux';
import {globalReducer} from './global';
import {contactReducer} from './contact';
import {createContactReducer} from './contact';

const reducer = combineReducers({contactReducer, createContactReducer, globalReducer})

export default reducer;
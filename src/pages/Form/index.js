import React from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {Button, TextInput, Gap} from '../../components';
import { createContactData, updateContactData } from '../../redux/action';
import {showMessage, useForm} from '../../utils';

const Form = ({navigation, route}) => {
  const {
    id,
    firstName,
    lastName,
    age,
  } = route.params;

  const [form, setForm] = useForm({   
    firstName: firstName,
    lastName: lastName,
    age: parseInt(age),
    photo: 'http://vignette1.wikia.nocookie.net/lotr/images/6/68/Bilbo_baggins.jpg/revision/latest?cb=20130202022550',
  });  

  const dispatch = useDispatch();

  const onSubmit = () => {
    if(id != 0){
      dispatch(updateContactData(id, form, navigation));
    }else{
      dispatch(createContactData(form, navigation));
    }
    
  };

  return (
    <ScrollView contentContainerStyle={styles.scroll}>
      <View style={styles.page}>
        <View style={styles.container}>
          <TextInput
            label="First Name"
            placeholder="Type your first name"
            value={form.firstName}
            onChangeText={(value) => setForm('firstName', value)}
          />
          <Gap height={16} />
          <TextInput
            label="Last Name"
            placeholder="Type your last name"
            value={form.lastName}
            onChangeText={(value) => setForm('lastName', value)}
          />
          <Gap height={16} />
          <TextInput
            label="Age"
            placeholder="Type your age"
            value={`${form.age}`}
            onChangeText={(value) => setForm('age', parseInt(value))}  
            keyboardType="numeric"          
          />
          <Gap height={24} />
          <Button text="Submit" onPress={onSubmit} />
        </View>
      </View>
    </ScrollView>
  );
};

export default Form;

const styles = StyleSheet.create({
  scroll: {flexGrow: 1},
  page: {flex: 1},
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingVertical: 26,
    flex: 1,
  },
  photo: {alignItems: 'center', marginTop: 26, marginBottom: 16},
  borderPhoto: {
    borderWidth: 1,
    borderColor: '#8D92A3',
    width: 110,
    height: 110,
    borderRadius: 110,
    borderStyle: 'dashed',
    justifyContent: 'center',
    alignItems: 'center',
  },
  photoContainer: {
    width: 90,
    height: 90,
    borderRadius: 90,
    backgroundColor: '#F0F0F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  addPhoto: {
    fontSize: 14,
    fontFamily: 'Nunito-Regular',
    color: '#8D92A3',
    textAlign: 'center',
  },
});

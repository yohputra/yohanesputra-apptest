import {StyleSheet, Text, View, Image} from 'react-native';
import React, { useEffect } from 'react';
import { IMGLogoContact } from '../../assets/images';

const Splash = ({navigation}) => {
  useEffect(() =>{
    setTimeout(() =>{
      navigation.replace('Home')
    }, 2000)
  }, [])
  return (
    <View style={styles.container}>
      <Image style={styles.logo}
              source={IMGLogoContact}
          />
      <Text style={styles.title_logo}>My Contact</Text>
    </View>
  );
}

export default Splash;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo:{
    width: 120,
    height: 120,
  },
  title_logo:{
    fontSize: 18,
    marginTop:15,
    fontFamily: 'Nunito-Bold',
    color: '#020202',
  }
});

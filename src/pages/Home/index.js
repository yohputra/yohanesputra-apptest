import { StyleSheet, Text, View, ScrollView, TouchableOpacity,Image } from 'react-native'; //Icon
import React, { useEffect, useState } from 'react';
import { ListItem } from '../../components';
import { useDispatch, useSelector } from 'react-redux';
import { getContactListData } from '../../redux/action';
import { ICAdd } from '../../assets/images';


const Home = ({ navigation }) => {
  const item = {
    id : 0,
    firstName : '',
    lastName: '',
    age: 0,
    photo : '',
  }
  const dispatch = useDispatch();
  const contact = useSelector((state) => state.contactReducer);

  useEffect(() => {
    dispatch(getContactListData());
  }, []);


  return (
    <View style={{flex:1}}>
      <ScrollView>
        <View style={styles.page}>
          {contact.data.map((item) => {
            return (
              <ListItem
                key={item.id}
                item={item}
              />
            );
          })}
        </View>
      </ScrollView>

      <TouchableOpacity
        style={styles.floating_button}
        onPress={() => navigation.navigate('Form', item)}>
        <Image
              style={styles.icon_action}
              source={ICAdd}
          />
      </TouchableOpacity>
    </View>


  );
};

export default Home;

const styles = StyleSheet.create({
  page: { flex: 1, marginBottom: 100 },
  floating_button:{
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 20,
    bottom: 20,
  },
  icon_action: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    overflow: "hidden",
    margin:5
  },
});

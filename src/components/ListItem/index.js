import {StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { deleteContactData } from '../../redux/action';
import { useDispatch } from 'react-redux';
import { ICDelete, ICEdit, ICUser } from '../../assets/images';

const ListItem = ({
    item
  }) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const onDeleteContact = (id) => {
    console.log(id)
    ////dispatch(setLoading(true));
    dispatch(deleteContactData(id, navigation));
    
  };

  return (
    <View style={styles.content}>
      { 
        item.photo == "N/A" ?
       <Image style={styles.photo} source={ICUser}/> : 
       <Image style={styles.photo} source={{uri:item.photo}} />
      }
      
      <View style={styles.text}>
        <Text style={styles.title}>{item.firstName+ " " +item.lastName}</Text> 
        <Text style={styles.title}>{item.age} years old</Text> 
      </View>   
      <View style={styles.section_action}>
        <TouchableOpacity activeOpacity = { .5 } onPress={() => navigation.navigate('Form', item)}>
        <Image
              style={styles.icon_action}
              source={ICEdit}
        />
        </TouchableOpacity>   
        <TouchableOpacity activeOpacity = { .5 } onPress={() => onDeleteContact(item.id)}>     
          <Image
              style={styles.icon_action}
              source={ICDelete}
          />
         </TouchableOpacity>   
      </View>
    </View>
  );
};

export default ListItem;

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: 'row',
    padding:10,
    borderBottomColor: '#d1d1d1',
    borderBottomWidth: 1,
  },
  title: {
    fontFamily: 'Nunito-Regular',
    fontSize: 16,
    color: '#020202',
  },
  photo: {
    width: 70,
    height: 70,
    borderRadius: 70 / 2,
    overflow: "hidden",
    borderWidth: 1,
    borderColor: "black"
  },
  section_action:{
    flex: 1, 
    flexDirection: 'row', 
    justifyContent:"flex-end",
    alignItems:"center"
  },
  text:{
    padding:10,
    flex:1
  },
  icon_action: {
    width: 25,
    height: 25,
    borderRadius: 25 / 2,
    overflow: "hidden",
    margin:5
  },
});

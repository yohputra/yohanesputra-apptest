import IMGLogoContact from './logo-contact.png';
import ICUser from './ic-user.png';
import ICEdit from './ic-pencil.png';
import ICDelete from './ic-delete.png';
import ICAdd from './ic-add.png';

export {IMGLogoContact, ICUser, ICEdit, ICDelete, ICAdd}
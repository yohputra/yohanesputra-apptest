import React from 'react';
import {createNativeStackNavigator } from '@react-navigation/native-stack'
import { Form, Home, Splash } from '../pages';

const Stack = createNativeStackNavigator();

const Router = () =>{
    return (
        <Stack.Navigator>
            <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}}/>
            <Stack.Screen name="Home" component={Home} options={{ title: 'My Contact' }}/>
            <Stack.Screen name="Form" component={Form} options={{ title: 'Add Contact' }}/>
        </Stack.Navigator>
    )
}

export default Router;